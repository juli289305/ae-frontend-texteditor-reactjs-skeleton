import React, { Component } from 'react';
import './FileZone.css';
import getMockText from '../text.service';

class FileZone extends Component {
    constructor(props) {
        super(props);
        this.state = {
            value: ''
        };
        this.getSimilarWorlds = this.getSimilarWorlds.bind(this)
    }
    componentDidMount() {
        getMockText().then((result) => this.setState({value : result}));
    }
    getSimilarWorlds(){
        if (window.getSelection().toString().trim().length !== 0){
            let word = window.getSelection().toString()
            fetch(`https://api.datamuse.com/words?ml=${word}&max=3`)
                .then(result => {
                    return result.json()
                }).then(data => {
                console.log(data)
            })

        }
    }
    render() {

        return (
            <div id="file-zone">
                <div id="file" onDoubleClick={this.getSimilarWorlds}>
                    {this.state.value}
                </div>
            </div>
        );
    }
}

export default FileZone;
