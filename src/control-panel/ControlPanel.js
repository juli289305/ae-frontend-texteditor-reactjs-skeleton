import React, {Component} from 'react';
import './ControlPanel.css';

class ControlPanel extends Component {
    constructor(props) {
        super(props);
        this.setStyle = this.setStyle.bind(this);
    }

    setStyle(e) {
        if (window.getSelection().toString().trim().length !== 0) {
            let span;
            let range = window.getSelection().getRangeAt(0);
            console.log(window.getSelection().getRangeAt(0).commonAncestorContainer.lastElementChild);
            if (!window.getSelection().getRangeAt(0).commonAncestorContainer.lastElementChild) {
                span = document.createElement('SPAN');
                let content = range.extractContents();
                span.appendChild(content);
                range.insertNode(span);
            } else {
                span = window.getSelection().getRangeAt(0).commonAncestorContainer.lastElementChild;
            }
            if (span.classList.contains(e.currentTarget.id))
            {
                span.classList.remove(e.currentTarget.id);
            }else{
                span.classList.add(e.currentTarget.id);
            }
        }
    }

    render() {
        return (
            <div id="control-panel">
                <div id="format-actions">
                    <button id='bold' className="format-action" type="button" onClick={this.setStyle}><b>B</b></button>
                    <button id='italic' className="format-action" type="button" onClick={this.setStyle}><i>I</i></button>
                    <button id='underline' className="format-action" type="button" onClick={this.setStyle}><u>U</u></button>
                </div>
            </div>
        );
    }
}

export default ControlPanel;
